# frozen_string_literal: true

class BlogsController < ApplicationController
  load_and_authorize_resource only: %i[show edit update destroy]
  skip_before_action :authenticate_user!, only: %i[index show]

  def index
    @blogs = Blog.order('name asc').paginate(page: params[:page], per_page: 20)
  end

  def show; end

  def new
    @blog = Blog.new
  end

  def create
    @blog = Blog.new(blog_params)
    @blog.user = current_user
    if @blog.save
      redirect_to blog_path(@blog),
                  notice: 'Blog was successfully created.'
    else
      render :new
    end
  end

  def edit
    authorize! :manage, @blog
  end

  def update
    authorize! :manage, @blog
    if @blog.update(blog_params)
      redirect_to blog_path(@blog),
                  notice: 'Blog was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    authorize! :manage, @blog
    if @blog.destroy
      redirect_to blogs_url, notice: 'Blog was successfully deleted.'
    else
      redirect_to blog_path(@blog),
                  notice: 'There was an error deleting the blog.'
    end
  end

  private

  def blog_params
    params.require(:blog).permit(%i[name description])
  end
end
