# frozen_string_literal: true

class WelcomeController < ApplicationController
  skip_before_action :authenticate_user!, only: %i[index]

  def index
    @posts = Post.order('updated_at desc').paginate(per_page: 20, page: params[:page])
  end
end
