# frozen_string_literal: true

class PostsController < ApplicationController
  load_and_authorize_resource only: %i[show edit update destroy]
  before_action :load_blog, only: %i[new create]
  skip_before_action :authenticate_user!, only: %i[index show]

  def index
    redirect_to blog_path(params[:blog_id])
  end

  def show; end

  def new
    @post = Post.new
  end

  def edit
    authorize! :manage, @post
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user
    @post.blog = @blog

    if @post.save
      redirect_to [@blog, @post], notice: 'Post was successfully created.'
    else
      render :new
    end
  end

  def update
    authorize! :manage, @post
    if @post.update(post_params)
      redirect_to [@post.blog, @post], notice: 'Post was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    authorize! :manage, @post
    @post.destroy
    redirect_to blog_path(@post.blog_id), notice: 'Post was successfully destroyed.'
  end

  private

  def load_blog
    @blog = Blog.find(params[:blog_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def post_params
    params.require(:post).permit(:title, :body)
  end
end
