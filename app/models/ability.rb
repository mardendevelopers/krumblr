# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    # permissions for every user, even if not logged in
    can :read, :all
    # additional permissions for logged in users (they can manage their blogs)
    can :manage, Blog, user_id: user.id if user.present?
    # additional permissions for logged in users (they can manage their posts)
    can :manage, Post, user_id: user.id if user.present?
  end
end
