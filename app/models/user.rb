# frozen_string_literal: true

class User < ApplicationRecord
  validates :full_name, presence: true
  validates :full_name, length: { maximum: 100 }
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
