# frozen_string_literal: true

class Blog < ApplicationRecord
  belongs_to :user
  has_many :posts
  validates :name, presence: true, uniqueness: true
  validates :user, presence: true
  validates_associated :user
  validates :name, length: { maximum: 50 }
end
