# frozen_string_literal: true

class Post < ApplicationRecord
  belongs_to :user
  belongs_to :blog
  validates :title, :body, presence: true
  validates :user, presence: true
  validates_associated :user
  validates :blog, presence: true
  validates_associated :blog
  validates :title, length: { maximum: 255 }
end
