# frozen_string_literal: true

module ApplicationHelper
  def flash_class(type)
    case type.to_sym
    when :notice then 'alert alert-primary'
    when :success then 'alert alert-success'
    when :error then 'alert alert-danger'
    when :alert then 'alert alert-warning'
    end
  end

  def blog_pagination_renderer
    @renderer = Class.new(WillPaginate::ActionView::LinkRenderer) do
      def previous_page
        num = @collection.current_page > 1 && @collection.current_page - 1
        previous_or_next_page(num, @options[:previous_label], 'btn-outline-secondary')
      end

      def next_page
        num = @collection.current_page < total_pages && @collection.current_page + 1
        previous_or_next_page(num, @options[:next_label], 'btn-outline-primary')
      end

      def previous_or_next_page(page, text, classname)
        if page
          link(text, page, class: ' btn ' + classname)
        else
          link(text, '#', class: ' btn disabled ' + classname)
        end
      end
    end
  end

  def blog_pagination(objects)
    if objects.total_pages > 1
      content_tag(:p) do
        concat page_entries_info objects, model: 'blog'
        concat will_paginate(objects, page_links: false, previous_label: 'Previous', next_label: 'Next', class: 'blog-pagination', renderer: blog_pagination_renderer)
      end
    end
  end

  def top_navigation
    content_tag(:div, class: 'nav-scroller py-1 mb-2') do
      content_tag(:nav, class: 'nav d-flex justify-content-between') do
        Blog.order('created_at asc').limit(10).each do |blog|
          concat link_to blog.name, blog_path(blog), class: 'p-2 text-muted'
        end
        concat link_to 'All Blogs', blogs_path, class: 'p-2 text-muted'
      end
    end
  end
end
