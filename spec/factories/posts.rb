# frozen_string_literal: true

FactoryBot.define do
  factory :post do
    title { Faker::Hobbit.quote }
    body { Faker::Lorem.paragraphs(3) }
    association :user, factory: :user
    association :blog, factory: :blog
  end
end
