# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:id, 1_000) { |sn| sn }
    full_name { Faker::Name.unique.name }
    email { Faker::Internet.unique.email }
    password 'password'
    password_confirmation 'password'
  end
end
