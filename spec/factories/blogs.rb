# frozen_string_literal: true

FactoryBot.define do
  factory :blog do
    name { Faker::Hobbit.unique.character }
    description { Faker::Hobbit.quote }
    association :user, factory: :user
  end
end
