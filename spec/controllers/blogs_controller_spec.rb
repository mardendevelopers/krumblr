# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BlogsController, type: :controller do
  let(:valid_attributes) do
    {
      name: 'Technology',
      description: 'Technology blog'
    }
  end

  let(:invalid_attributes) { { name: '' } }

  login_user

  describe 'GET #index' do
    it 'assigns all blogs as @blogs' do
      blog = Blog.new valid_attributes
      blog.user = subject.current_user
      blog.save

      valid_attributes2 = {
        name: 'Arts',
        description: 'Arts blog'
      }
      blog2 = Blog.new valid_attributes2
      blog2.user = create(:user)
      blog2.save

      get :index
      expect(assigns(:blogs)).to include(blog)
      expect(assigns(:blogs)).to include(blog2)
    end
  end

  describe 'GET #show' do
    it "assigns the requested blog as @blog if it's mine" do
      blog = Blog.new valid_attributes
      blog.user = subject.current_user
      blog.save
      get :show, params: { id: blog.to_param }
      expect(assigns(:blog)).to eq(blog)
    end

    it "assigns the requested blog as @blog even if isn't mine" do
      blog = Blog.new valid_attributes
      blog.user = create(:user)
      blog.save
      get :show, params: { id: blog.to_param }
      expect(assigns(:blog)).to eq(blog)
    end
  end

  describe 'GET #new' do
    it 'assigns a new blog as @blog' do
      get :new
      expect(assigns(:blog)).to be_a_new(Blog)
    end
  end

  describe 'GET #edit' do
    it "assigns the requested blog as @blog if it's mine" do
      blog = Blog.new valid_attributes
      blog.user = subject.current_user
      blog.save
      get :edit, params: { id: blog.to_param }
      expect(assigns(:blog)).to eq(blog)
    end

    it "raise authorisation error if the requested blog isn't mine" do
      blog = Blog.new valid_attributes
      blog.user = create(:user)
      blog.save
      expect  do
        get :edit, params: { id: blog.to_param }
      end.to raise_exception(CanCan::AccessDenied)
    end
  end

  describe 'GET #update' do
    it "assigns the requested blog as @blog if it's mine" do
      blog = Blog.new valid_attributes
      blog.user = subject.current_user
      blog.save
      get :update, params: { id: blog.to_param, blog: valid_attributes }
      expect(assigns(:blog)).to eq(blog)
    end

    it "updated the requested blog as @blog if it's mine" do
      blog = Blog.new valid_attributes
      blog.user = subject.current_user
      blog.save
      update_attributes = { description: 'Technology updates' }
      get :update, params: { id: blog.to_param, blog: update_attributes }
      blog.reload
      expect(blog.description).to eq(update_attributes[:description])
    end

    it "raise authorisation error if the requested blog isn't mine" do
      blog = Blog.new valid_attributes
      blog.user = create(:user)
      blog.save
      expect do
        get :update, params: { id: blog.to_param, blog: valid_attributes }
      end.to raise_exception(CanCan::AccessDenied)
    end
  end

  describe 'GET #destroy' do
    it "assigns the requested blog as @blog if it's mine" do
      blog = Blog.new valid_attributes
      blog.user = subject.current_user
      blog.save
      get :destroy, params: { id: blog.to_param }
      expect(Blog.where(id: blog.id).first).to be_nil
    end

    it "raise authorisation error if the requested blog isn't mine" do
      blog = Blog.new valid_attributes
      blog.user = create(:user)
      blog.save
      expect do
        get :destroy, params: { id: blog.to_param }
      end.to raise_exception(CanCan::AccessDenied)
    end
  end
end
