# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  # This should return the minimal set of attributes required to create a valid
  # Post. As you add validations to Post, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) do
    {
      title: 'Technology advancements in recent era',
      body: 'dummy text'
    }
  end

  let(:invalid_attributes) do
    {
      title: ''
    }
  end

  before(:each) do
    @blog = create(:blog)
  end

  def create_post
    post = Post.new valid_attributes
    post.user = subject.current_user
    post.blog = @blog
    post.save
    post
  end

  login_user

  describe 'GET #index' do
    it 'redirects to blog #show page' do
      post = create_post
      get :index, params: { blog_id: @blog.to_param }
      expect(response).to redirect_to(@blog)
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      post = create_post
      get :show, params: { blog_id: @blog.to_param, id: post.to_param }
      expect(response).to be_success
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: { blog_id: @blog.to_param }
      expect(response).to be_success
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      post = create_post
      get :edit, params: { blog_id: @blog.to_param, id: post.to_param }
      expect(response).to be_success
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Post' do
        expect do
          post :create, params: { blog_id: @blog.to_param, post: valid_attributes }
        end.to change(Post, :count).by(1)
      end

      it 'redirects to the created post' do
        post :create, params: { blog_id: @blog.to_param, post: valid_attributes }
        expect(response).to redirect_to([@blog, Post.last])
      end

      it 'should not use user_id and blog_id params a new Post' do
        user2 = create(:user)
        blog2 = create(:blog)
        post :create, params: {
          blog_id: @blog.to_param,
          post: valid_attributes.merge(
            user_id: user2.id,
            blog_id: blog2.id
          )
        }
        post = Post.last
        expect(post.user).not_to eq(user2)
        expect(post.blog).not_to eq(blog2)

        expect(post.user).to eq(subject.current_user)
        expect(post.blog).to eq(@blog)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { blog_id: @blog.to_param, post: invalid_attributes }
        expect(response).to be_success
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        {
          title: 'new post'
        }
      end

      it 'updates the requested post' do
        post = create_post
        put :update, params: { blog_id: @blog.to_param, id: post.to_param, post: new_attributes }
        post.reload
        expect(post.title).to eq('new post')
      end

      it 'redirects to the post' do
        post = create_post
        put :update, params: { blog_id: @blog.to_param, id: post.to_param, post: valid_attributes }
        expect(response).to redirect_to([@blog, post])
      end

      it 'should not use user_id and blog_id params while updating Post' do
        post = create_post
        user2 = create(:user)
        blog2 = create(:blog)
        put :update, params: {
          blog_id: @blog.to_param,
          id: post.to_param,
          post: valid_attributes.merge(
            user_id: user2.id,
            blog_id: blog2.id
          )
        }
        post.reload
        expect(post.user).not_to eq(user2)
        expect(post.blog).not_to eq(blog2)

        expect(post.user).to eq(subject.current_user)
        expect(post.blog).to eq(@blog)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        post = create_post
        put :update, params: { blog_id: @blog.to_param, id: post.to_param, post: invalid_attributes }
        expect(response).to be_success
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested post' do
      post = create_post
      expect do
        delete :destroy, params: { blog_id: @blog.to_param, id: post.to_param }
      end.to change(Post, :count).by(-1)
    end

    it 'redirects to the posts list' do
      post = create_post
      delete :destroy, params: { blog_id: @blog.to_param, id: post.to_param }
      expect(response).to redirect_to(blog_path(@blog))
    end
  end
end
