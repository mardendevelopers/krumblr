# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do
  describe '#authenticate_user' do
    controller do
      def index
        render plain: 'Secure Hello World'
      end
    end

    it 'should redirect to login if user is not authenticated and trying to access secure page' do
      get :index
      expect(response).to redirect_to(new_user_session_path)
    end

    it 'should render page if user is authenticated' do
      sign_in FactoryBot.create(:user)
      get :index
      expect(response.body).to eq('Secure Hello World')
    end
  end

  describe 'skip_before_action #authenticate_user' do
    controller do
      skip_before_action :authenticate_user!
      def index
        render plain: 'Hello World'
      end
    end

    it 'should not redirect to login if user is not authenticated and trying to access public page' do
      get :index
      expect(response.body).to eq('Hello World')
    end

    it 'should also render public page for logged in users' do
      sign_in FactoryBot.create(:user)
      get :index
      expect(response.body).to eq('Hello World')
    end
  end
end
