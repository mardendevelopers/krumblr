# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Post, type: :model do
  it 'is not valid without a title' do
    should validate_presence_of(:title)
  end

  it 'is not valid without a body' do
    should validate_presence_of(:body)
  end

  it 'is not valid without a user' do
    should validate_presence_of(:user)
  end

  it 'is not valid without a blog' do
    should validate_presence_of(:blog)
  end

  it 'it is valid with valid_attributes' do
    post = Post.new(
      title: 'Usage of HPC in Genomics',
      body: 'Dummy text',
      user: build(:user),
      blog: build(:blog)
    )
    expect(post.valid?).to be true
  end
end
