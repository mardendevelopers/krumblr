# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it 'is not valid without a full_name' do
    should validate_presence_of(:full_name)
  end

  it 'is not valid without a email' do
    should validate_presence_of(:email)
  end

  it 'is not valid without a password' do
    should validate_presence_of(:password)
  end

  it 'is not valid without a case insensitive unique email' do
    should validate_uniqueness_of(:email).case_insensitive
  end

  it 'is not valid without a case insensitive unique email' do
    should validate_uniqueness_of(:email).case_insensitive
  end

  it 'it is valid with valid_attributes' do
    @user = User.new(
      full_name: 'First Last',
      email: 'test@email.com',
      password: 'password',
      password_confirmation: 'password'
    )
    expect(@user.valid?).to be true
  end
end
