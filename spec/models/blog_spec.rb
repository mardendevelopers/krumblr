# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Blog, type: :model do
  it 'is not valid without a name' do
    should validate_presence_of(:name)
  end

  it 'is not valid without a user' do
    should validate_presence_of(:user)
  end

  it 'is not valid without unique name' do
    should validate_uniqueness_of(:name)
  end

  it 'it is valid with valid_attributes' do
    blog = Blog.new(
      name: 'Technology',
      description: 'Technology blog',
      user: build(:user)
    )
    expect(blog.valid?).to be true
  end
end
