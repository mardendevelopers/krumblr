Rails.application.routes.draw do
  resources :blogs do
    resources :posts
  end
  devise_for :users
  root to: 'welcome#index'
end
